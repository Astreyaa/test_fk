<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%ttx_players}}`.
 */
class m230829_231740_create_ttx_players_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('ttx_players', [
            'id' => $this->primaryKey(),
            'player_id' => $this->integer()->notNull()->comment('Ссылка на идентификатор игрока'),
            'ttx_id' => $this->integer()->notNull()->comment('Ссылка на идентификатор характеристики'),
            'ttx_value' => $this->integer()->check('ttx_value >= 1 and ttx_value <= 3')->comment('Значение характеристики'),
        ]);

        $this->addForeignKey(
            'fk_ttx_players_player_id',
            '{{%Ttx_players}}',
            'player_id',
            '{{%Players}}',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_ttx_players_ttx_id',
            '{{%Ttx_players}}',
            'ttx_id',
            '{{%Ttx_names}}',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%ttx_players}}');
    }
}
