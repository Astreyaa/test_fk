<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%ttx_names}}`.
 */
class m230829_231725_create_ttx_names_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('ttx_names', [
            'id' => $this->primaryKey(),
            'ttx_name' => $this->string(60)->notNull()->comment('Название характеристики'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('ttx_names');
    }
}
