<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%players}}`.
 */
class m230829_231620_create_players_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('players',[
            'id' => $this->primaryKey(),
            'name' => $this->string(60)->notNull()->comment('Имя игрока'),
            'positions' => $this->string(30)->comment('Позиция'),
            'birthday' => $this->date()->notNull()->comment('День рождения'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%players}}');
    }
}
