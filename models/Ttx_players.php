<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Ttx_players".
 *
 * @property int $id Идентификатор характеристики игрока
 * @property int $player_id Ссылка на идентификатор игрока
 * @property int $ttx_id Ссылка на идентификатор характеристики
 * @property int $ttx_value
 */
class Ttx_players extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Ttx_players';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['player_id'], 'required'],
            [['player_id', 'ttx_id', 'ttx_value'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Идентификатор характеристики игрока',
            'player_id' => 'Ссылка на идентификатор игрока',
            'ttx_id' => 'Ссылка на идентификатор характеристики',
            'ttx_value' => 'Значение',
        ];
    }
}
