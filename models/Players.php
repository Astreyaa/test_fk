<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Players".
 *
 * @property int $id Идентификатор игрока
 * @property string $name Имя игрока
 * @property string $positions Позиция
 * @property string $birthday День рождения
 */
class Players extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Players';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'positions', 'birthday'], 'required'],
            [['birthday'], 'safe'],
            [['name'], 'string', 'max' => 60],
            [['positions'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Идентификатор игрока',
            'name' => 'Имя игрока',
            'positions' => 'Позиция',
            'birthday' => 'День рождения',
        ];
    }
}
