<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Ttx_names".
 *
 * @property int $id Идентификатор характеристики
 * @property string $ttx_name Название характеристики
 */
class Ttx_names extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Ttx_names';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ttx_name'], 'required'],
            [['ttx_name'], 'string', 'max' => 60],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Идентификатор характеристики',
            'ttx_name' => 'Название характеристики',
        ];
    }
}
