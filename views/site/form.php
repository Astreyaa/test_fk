<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
?>
<?php $form = ActiveForm::begin(); ?>

<?php 
    $players = app\models\Players::find()->all();
    
    $items = ArrayHelper::map($players,'id','name');
    $params = [ 
    ];
    echo $form->field($model, 'player_id')->dropDownList($items,$params);

    $players = app\models\Ttx_names::find()->all();
    
    $items = ArrayHelper::map($players,'id','ttx_name');
    $params = [ 
    ];
    echo $form->field($model, 'ttx_id')->dropDownList($items,$params);
?>
<button type="button" class="btn btn-primary add_char mb-3">+</button>
<div class="d-none ttx_values">
    <?
    echo $form->field($model, 'ttx_value', )->input('ttx_value', ['multiple'=>true, 'type' => 'password',  'maxlength' => 1, ]);
?>
</div>


    <div class="form-group">
        <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>


